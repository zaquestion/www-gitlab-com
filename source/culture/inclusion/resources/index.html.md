---
layout: markdown_page
title: "Inclusion Resources"
---

### Salesforce Inclusion Training

[Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.  
   * [Business Value of Equality](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) (This module has two units. The second is specific to Salesforce values and mission and is not required or suggested for our training.)
   * [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
   * [Diversity and Incusion Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_be_more_inclusive)
   * [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)
